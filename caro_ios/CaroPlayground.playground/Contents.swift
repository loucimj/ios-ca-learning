//: Playground - noun: a place where people can play

import UIKit

var str = "Hello World"

class User {
    init () {
        
    }
    
    func getName() -> String? {
        return "mi nombre"
    }
    
}

class Service {
    
    var serviceName:String?
    
    init () {
        
    }
    convenience init(serviceName:String) {
        self.init()
        self.serviceName = serviceName
    }
    
    func getUserByID(id:Int) -> User? {
        return User()
    }
    
}

let serviceArray:Array<Service> = Array<Service>()

let service1:Service = Service()
let service2 = Service(serviceName: "getUsuarios")

print(service1.serviceName)
print(service2.serviceName!)

let user = service1.getUserByID(1)

if let name = user?.getName() {
    print (name)
} else {
    print ("nil")
}




